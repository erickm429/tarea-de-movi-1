package com.example.erick.conversor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText cent, fare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.cent = (EditText)findViewById(R.id.txtcentigrados);
        this.fare = (EditText)findViewById(R.id.txtFarenheit);

        this.cent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                float faren;
                faren = (1.8f)*Float.parseFloat(cent.getText().toString())+32;
                fare.setText(""+faren);

                return false;
            }
        });
        this.fare.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                float centi;
                centi = (Float.parseFloat(fare.getText().toString())-32)/(1.8f);
                cent.setText(""+centi);
                return false;
            }
        });

    }
}
